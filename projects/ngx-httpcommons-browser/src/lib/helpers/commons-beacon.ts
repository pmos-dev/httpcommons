export abstract class CommonsBeacon {
	public static hasBrowserSupport(): boolean {
		return navigator !== undefined
				&& 'object' === typeof navigator
				&& navigator.sendBeacon !== undefined
				&& 'function' === typeof navigator.sendBeacon;
	}
	
	public static sendBeacon(url: string, data: any): boolean {
		if (!CommonsBeacon.hasBrowserSupport()) return false;
		return navigator.sendBeacon(url, data);
	}
}
