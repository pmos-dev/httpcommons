import { EventEmitter } from '@angular/core';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import {
		ICommonsNetworkPacket,
		isICommonsNetworkPacket,
		ICommonsNetworkPacketCompressed,
		isICommonsNetworkPacketCompressed,
		compressPacket,
		decompressPacket
} from 'tscommons-network';
import { ICommonsNetworkHandshake } from 'tscommons-network';

import { CommonsSocketIoClientService } from 'ngx-httpcommons-socket-io';
import { CommonsSocketIoUrlOptions } from 'ngx-httpcommons-socket-io';

import { CommonsNetworkService } from './commons-network.service';

class SocketIoClientService extends CommonsSocketIoClientService {
	constructor(
			url: string,
			private callback: (data: unknown) => void
	) {
		super(
				CommonsSocketIoUrlOptions.buildUrl(url),
				true,
				CommonsSocketIoUrlOptions.buildOptions(url)
		);
	}

	protected setupOns(): void {
		this.on('rx', (data: unknown): void => {
			this.callback(data);
		});
	}
	
	public async emit(command: string, data: any): Promise<unknown> {
		return await super.emit(command, data);
	}
}

export class CommonsNetworkSocketIoService extends CommonsNetworkService {
	socketIoClientService: SocketIoClientService;
	
	constructor(
			url: string,
			private compress: boolean = false
	) {
		super();
		
		this.socketIoClientService = new SocketIoClientService(
				url,
				(data: unknown): void => {
					if (!CommonsType.isEncodedObject(data)) {
						console.log('Invalid encoded packet');
						return;
					}
					
					const decoded: TPropertyObject = CommonsType.decodePropertyObject(data);

					let packet: ICommonsNetworkPacket;
					
					if (this.compress) {
						if (!isICommonsNetworkPacketCompressed(decoded)) {
							console.log('Invalid compressed packet');
							return;
						}
						
						packet = decompressPacket(decoded);
					} else {
						if (!isICommonsNetworkPacket(decoded)) {
							console.log('Invalid packet');
							return;
						}
						
						packet = decoded;
					}
					
					this.incoming(packet);
				}
		);
	}

	public connectObservable(): EventEmitter<void> {
		return this.socketIoClientService.connectObservable();
	}

	public disconnectObservable(): EventEmitter<void> {
		return this.socketIoClientService.disconnectObservable();
	}
	
	public async transmit(packet: ICommonsNetworkPacket): Promise<unknown> {
		let encoded: TEncodedObject;
		
		if (this.compress) {
			const compressed: ICommonsNetworkPacketCompressed = compressPacket(packet);
			encoded = CommonsType.encodePropertyObject(compressed);
		} else encoded = CommonsType.encodePropertyObject(packet);
		
		return await this.socketIoClientService.emit('tx', encoded);
	}
	
	public async requestReplay(): Promise<void> {
		const since: string|undefined = this.getLastSeen();
		if (since === undefined) return;
		
		try {
			const lastSeen: unknown = await this.socketIoClientService.emit('replay', {
				since: since,
				channel: this.getChannel()!,
				deviceId: this.getDeviceId()!
			});
			if (CommonsType.isString(lastSeen)) {
				this.setLastSeen(lastSeen);
			}
		} catch (e) {
			console.log('Replay request failed');
		}
		return;
	}

	public connect(params?: ICommonsNetworkHandshake): boolean {
		if (this.getDeviceId() === undefined) throw new Error('No deviceId set prior to connecting');
		if (this.getChannel() === undefined) throw new Error('No channel set prior to connecting');

		if (params === undefined) {
			params = {
					channel: this.getChannel()!,
					deviceId: this.getDeviceId()!
			};
		} else {
			params.channel = this.getChannel()!;
			params.deviceId = this.getDeviceId()!;
		}
				
		return this.socketIoClientService.connect(params);
	}
}
