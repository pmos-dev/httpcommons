export enum ECommonsSignalStrength {
	DISCONNECTED = 'disconnected',
	LOW = 'low',
	MEDIUM = 'medium',
	HIGH = 'high'
}

export function toECommonsSignalStrength(strength: number): ECommonsSignalStrength|undefined {
	if (strength > 2) return ECommonsSignalStrength.HIGH;
	if (strength === 2) return ECommonsSignalStrength.MEDIUM;
	if (strength === 1) return ECommonsSignalStrength.LOW;
	
	return ECommonsSignalStrength.DISCONNECTED;
}

export function fromECommonsSignalStrength(strength: ECommonsSignalStrength): number {
	switch (strength) {
		case ECommonsSignalStrength.DISCONNECTED:
			return 0;
		case ECommonsSignalStrength.LOW:
			return 1;
		case ECommonsSignalStrength.MEDIUM:
			return 2;
		case ECommonsSignalStrength.HIGH:
			return 3;
	}
}
