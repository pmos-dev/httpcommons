/*
 * Public API Surface of ngx-httpcommons-network
 */

export * from './lib/enums/ecommons-signal-strength';

export * from './lib/services/commons-network.service';
export * from './lib/services/commons-network-socket-io.service';
export * from './lib/services/commons-network-multi.service';
export * from './lib/services/commons-network-multi-socket-io.service';

export * from './lib/classes/commons-remote-control';

export * from './lib/ngx-httpcommons-network.module';
