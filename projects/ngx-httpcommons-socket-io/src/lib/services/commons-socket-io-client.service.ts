import { EventEmitter } from '@angular/core';

import { Subscription } from 'rxjs';
import * as io from 'socket.io-client';

export abstract class CommonsSocketIoClientService {
	private connectEmitter: EventEmitter<void> = new EventEmitter<void>(true);
	private disconnectEmitter: EventEmitter<void> = new EventEmitter<void>(true);

	private socket?: SocketIOClient.Socket;
	private connected: boolean = false;
	private enabled: boolean|undefined;

	constructor(
			private url: string,
			private defaultEnabled?: boolean|undefined,
			private options?: SocketIOClient.ConnectOpts
	) {}

	public connectObservable(): EventEmitter<void> {
		return this.connectEmitter;
	}

	public disconnectObservable(): EventEmitter<void> {
		return this.disconnectEmitter;
	}

	public connect(params?: {}|undefined): boolean {
		if (this.socket) return true;	// already connected

		const options: SocketIOClient.ConnectOpts = this.options || {};
		
		if (params) {
			const query: string[] = [];
			for (const key of Object.keys(params)) {
				query.push(`${key}=${params[key]}`);
			}
			
			options.query = query.join('&');
		}

		this.socket = io.connect(this.url, options);
		if (!this.socket) return false;
		
		this.socket.on('connect', (): void => {
			this.connected = true;
			if (this.enabled === undefined) this.enabled = this.defaultEnabled === false ? false : true;
			this.connectEmitter.emit();
		});
		this.socket.on('disconnect', (): void => {
			this.connected = false;
			this.disconnectEmitter.emit();
		});

		this.setupOns();
		
		return true;
	}
	
	public connectAndAwait(timeout: number, params?: {}|undefined): Promise<boolean> {
		return new Promise((resolve: (_: boolean) => void, _): void => {
			if (!this.connect(params)) {
				resolve(false);
				return;
			}
			
			if (this.connected) {
				resolve(true);
				return;
			}
			
			let subscriptionConnect: Subscription|undefined;
			let subscriptionDisconnect: Subscription|undefined;
			let subscriptionTimeout: any|undefined;
			
			subscriptionConnect = this.connectEmitter.subscribe(
					(): void => {
						if (subscriptionTimeout) clearTimeout(subscriptionTimeout);
						if (subscriptionConnect) subscriptionConnect.unsubscribe();
						if (subscriptionDisconnect) subscriptionDisconnect.unsubscribe();
						resolve(true);
					}
			);
			
			subscriptionDisconnect = this.disconnectEmitter.subscribe(
					(): void => {
						if (subscriptionTimeout) clearTimeout(subscriptionTimeout);
						if (subscriptionConnect) subscriptionConnect.unsubscribe();
						if (subscriptionDisconnect) subscriptionDisconnect.unsubscribe();
						resolve(false);
					}
			);
			
			subscriptionTimeout = setTimeout((): void => {
				if (subscriptionTimeout) clearTimeout(subscriptionTimeout);
				if (subscriptionConnect) subscriptionConnect.unsubscribe();
				if (subscriptionDisconnect) subscriptionDisconnect.unsubscribe();
				resolve(false);
			}, timeout);
		});
	}

	public disconnect(): void {
		if (this.socket === undefined) return;
		this.socket.disconnect();
	}

	protected abstract setupOns(): void;

	public setEnabled(state: boolean): void {
		this.enabled = state;
	}
	
	public getEnabled(): boolean {
		return this.enabled ? true : false;
	}

	public isConnected(): boolean {
		return this.socket !== undefined && this.connected;
	}

	protected on(
			command: string,
			callback: (data: unknown) => void
	): void {
		if (!this.socket) return;	// shouldn't really be possible if the class is being used properly with setupOns
		
		this.socket.on(
				command,
				(data: unknown): void => {
					if (!this.isConnected()) return;	// is this possible?
					if (!this.getEnabled()) return;
					
					callback(data);
				}
		);
	}

	protected async emit(
			command: string,
			data: any
	): Promise<unknown> {
		return new Promise((resolve: (_: unknown) => void, reject): void => {
			if (this.socket === undefined) {
				reject('No socket yet');
				return;
			}
		
			if (!this.isConnected()) {
				reject('Not connected');
				return;
			}
		
			if (!this.getEnabled()) {
				reject('Not enabled');
				return;
			}
		
			this.socket.emit(command, data, (response: unknown): void => {
				resolve(response);
			});
		});
	}
}
