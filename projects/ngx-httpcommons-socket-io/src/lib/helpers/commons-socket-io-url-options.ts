export abstract class CommonsSocketIoUrlOptions {
// original, may need to be restored if the new one from NodeAppCommonsAngular breaks things
//	public static buildOptions(url: string): SocketIOClient.ConnectOpts {
//		const options: SocketIOClient.ConnectOpts = {
//				transports: [ 'websocket', 'polling' ]
//		};
//		
//		const regexp: RegExp = /^http(?:s?):\/\/[^\/]+\/(.+)$/i;
//		const match: RegExpExecArray|null = regexp.exec(url);
//		
//		if (match !== null) {
//			options.path = `/${match[1]}${match[1].endsWith('/') ? '' : '/'}socket.io`;
//		}
//		
//		return options;
//	}
//
//	public static buildUrl(url: string): string {
//		const regexp: RegExp = /^(http(?:s?):\/\/[^\/]+\/).+$/i;
//		const match: RegExpExecArray|null = regexp.exec(url);
//		
//		if (match !== null) return match[1];
//		
//		return url;
//	}

	public static buildOptions(url: string): SocketIOClient.ConnectOpts {
		const options: SocketIOClient.ConnectOpts = {
				transports: [ 'websocket', 'polling' ]
		};

		if (url.startsWith('/')) {
			options.path = `${url}${url.endsWith('/') ? '' : '/'}socket.io`;
		} else {
			const regexp: RegExp = /^http(?:s?):\/\/[^\/]+\/(.+)$/i;
			const match: RegExpExecArray|null = regexp.exec(url);
			
			if (match !== null) {
				options.path = `/${match[1]}${match[1].endsWith('/') ? '' : '/'}socket.io`;
			}
		}
		
		return options;
	}

	public static buildUrl(url: string): string {
		if (url.startsWith('/')) return '/';
		
		const regexp: RegExp = /^(http(?:s?):\/\/[^\/]+\/).+$/i;
		const match: RegExpExecArray|null = regexp.exec(url);
		
		if (match !== null) return match[1];
		
		return url;
	}
}
