import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { timeout as rxjsTimeout, take } from 'rxjs/operators';

import { TEncodedObject, TEncoded } from 'tscommons-core';
import { CommonsHttpError } from 'tscommons-http';
import { CommonsHttpTimeoutError } from 'tscommons-http';
import { CommonsHttpResponses } from 'tscommons-http';
import { ICommonsHttpClientImplementation } from 'tscommons-http';
import { THttpHeaderOrParamObject } from 'tscommons-http';
import { TCommonsHttpInternalRequestOptions } from 'tscommons-http';
import { ECommonsHttpContentType } from 'tscommons-http';
import { ECommonsHttpMethod, fromECommonsHttpMethod } from 'tscommons-http';
import { ECommonsHttpResponseDataType } from 'tscommons-http';

// this is exported from this file, but not exported in the public-api.ts
// it's an internal class used by the two services

@Injectable({
		providedIn: 'root'
})
export class CommonsInternalHttpClientImplementationService implements ICommonsHttpClientImplementation {
	private static encodeBody(data: TEncodedObject, contentType: ECommonsHttpContentType): TEncodedObject|string {
		switch (contentType) {
			case ECommonsHttpContentType.JSON:
				return data;
			case ECommonsHttpContentType.FORM_URL:
				let hp: HttpParams = new HttpParams();
				
				if (data) {
					for (const key of Object.keys(data)) {
						const value: TEncoded = data[key];
						
						if (value === null) {
							hp = hp.set(key, '');
						} else {
							hp = hp.set(key, value.toString());
						}
					}
				}
				
				return hp.toString();
		}
		
		throw new Error('Unknown encoding type');
	}

	private static async request(
			httpClient: HttpClient,
			method: ECommonsHttpMethod,
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyData: TEncodedObject|string,
			returnResult: boolean,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): Promise<string|Uint8Array|void> {
		const options: {
				body?: TEncodedObject|string;
				headers?: THttpHeaderOrParamObject;
				params?: THttpHeaderOrParamObject;
				responseType: 'arraybuffer' | 'text';
		} = {
				headers: headers,
				params: params,
				responseType: responseDataType === ECommonsHttpResponseDataType.UINT8ARRAY ? 'arraybuffer' : 'text'
		};
		
		switch (method) {
			case ECommonsHttpMethod.POST:
			case ECommonsHttpMethod.PUT:
			case ECommonsHttpMethod.PATCH:
				options.body = bodyData;
		}
		
		let request: Observable<ArrayBuffer|string> = httpClient.request(
				fromECommonsHttpMethod(method),
				url,
				options
		);
		
		if (timeout) request = request.pipe(rxjsTimeout(timeout));

		try {
			const responseData: ArrayBuffer|string = await (request
					.pipe(take(1))
					.toPromise());
	
			if (!returnResult) return;
			
			if (responseDataType === ECommonsHttpResponseDataType.UINT8ARRAY) {
				return new Uint8Array(responseData as ArrayBuffer);
			}
	
			return responseData.toString();
		} catch (e) {
			const isTimeout: boolean = e.message && /timeout/i.test(e.message);
			if (isTimeout) throw new CommonsHttpTimeoutError();
			
			if (e instanceof HttpErrorResponse) {
				const httpError: CommonsHttpError = CommonsHttpResponses.buildErrorFromResponseCode(e.status, e.message || e.error.message);
				throw httpError;
			}
			
			throw e;
		}
	}
	
	private static requestHeadGetDelete(
			httpClient: HttpClient,
			method: ECommonsHttpMethod,
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			returnResult: boolean,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): Promise<any|void> {
		return CommonsInternalHttpClientImplementationService.request(
				httpClient,
				method,
				url,
				params,
				headers,
				'',
				returnResult,
				responseDataType,
				timeout
		);
	}

	private static requestPostPutPatch(
			httpClient: HttpClient,
			method: ECommonsHttpMethod,
			url: string,
			body: TEncodedObject,
			contentType: ECommonsHttpContentType,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			returnResult: boolean,
			responseDataType: ECommonsHttpResponseDataType,
			timeout: number|undefined
	): Promise<any|void> {
		return CommonsInternalHttpClientImplementationService.request(
				httpClient,
				method,
				url,
				params,
				headers,
				CommonsInternalHttpClientImplementationService.encodeBody(body, contentType),
				returnResult,
				responseDataType,
				timeout
		);
	}

	constructor(
			private http: HttpClient
	) {}
	
	public internalHead(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			options: TCommonsHttpInternalRequestOptions
	): Promise<void> {
		return CommonsInternalHttpClientImplementationService.requestHeadGetDelete(
				this.http,
				ECommonsHttpMethod.HEAD,
				url,
				params,
				headers,
				false,
				ECommonsHttpResponseDataType.UINT8ARRAY,	// na
				options.timeout
		);
	}

	public internalGet(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementationService.requestHeadGetDelete(
				this.http,
				ECommonsHttpMethod.GET,
				url,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		);
	}

	public internalDelete(
			url: string,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementationService.requestHeadGetDelete(
				this.http,
				ECommonsHttpMethod.DELETE,
				url,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		);
	}
	
	public internalPost<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementationService.requestPostPutPatch(
				this.http,
				ECommonsHttpMethod.POST,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		);
	}
	
	public internalPut<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementationService.requestPostPutPatch(
				this.http,
				ECommonsHttpMethod.PUT,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		);
	}
	
	public internalPatch<
			B extends TEncodedObject = TEncodedObject
	>(
			url: string,
			body: B,
			params: THttpHeaderOrParamObject,
			headers: THttpHeaderOrParamObject,
			bodyDataEncoding: ECommonsHttpContentType,
			responseDataType: ECommonsHttpResponseDataType,
			options: TCommonsHttpInternalRequestOptions
	): Promise<string|Uint8Array> {
		return CommonsInternalHttpClientImplementationService.requestPostPutPatch(
				this.http,
				ECommonsHttpMethod.PATCH,
				url,
				body,
				bodyDataEncoding,
				params,
				headers,
				true,
				responseDataType,
				options.timeout
		);
	}
}
