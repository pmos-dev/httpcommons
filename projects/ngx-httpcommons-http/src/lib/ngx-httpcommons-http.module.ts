import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommonsInternalHttpClientImplementationService } from './services/commons-internal-http-client-implementation.service';

@NgModule({
		imports: [
				CommonModule
		],
		declarations: []
})
export class NgxHttpCommonsHttpModule {
	static forRoot(): ModuleWithProviders<NgxHttpCommonsHttpModule> {
		return {
				ngModule: NgxHttpCommonsHttpModule,
				providers: [
						CommonsInternalHttpClientImplementationService
				]
		};
	}
}
